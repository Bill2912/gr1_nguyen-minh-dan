import numpy as np 
x = np.array([1, 2, 3])
y = np.array([1, 2, 4], dtype = np.float64)
print(x, y)
# help(np.array)
# numpy zeros 
zero = np.zeros(3)
print(zero)

one = np.ones(5)
print(one)
# special numpy have like chieu va bien
a = np.array(range(10))
print(a)
a_zero = np.zeros_like(a)
a_one = np.ones_like(a)
print(a_zero, a_one)


z = np.arange(10)
print(a is z)



a_cong = np.arange(1, 10, 1)
b_tru = np.arange(10, 1, -1)
print(a_cong, b_tru)
