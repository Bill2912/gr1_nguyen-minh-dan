*Lướt qua về lịch sử phát triển của hệ gợi ý...*

1990

-----
* Ban đầu : kỹ thuật automation-techniques-algorithms được ra đời. Xác định người dùng có sở thích tương tự, sắp xếp xếp hạng họ với nhau thanhf một mức trung bình được cá nhân hóa
* Rapid commercialization - the challenges of scale and value ( Được thương mại hóa rộng rãi, nhanh chóng tạo ra những thách thức về quy mô và giá trị)
* Điều đó có nghĩa là muốn tồn tại , kiếm được tiền thì phải có thể đưa ra nghiên cứu tốt hơn , chính xác hơn , có hiệu quả kinh tế cao hơn  mà không làm chậm những thứ sẵn có 

-----
2000-2005 (Research explosion-recommenders go mainstream)


-----


* Nghiên cứu về hệ gợi ý đến từ nhiều người thuộc nhiều ngành khác nhau , ví dụ : trí tuệ nhân tạo , information retrieval , data mining , AI , security and privacy, bussiness , marketing reseach.
* Việc dần dần chúng ta có những bộ dữ liệu lớn , và có những cuộc thi ví dụ Netflix Prize cho hệ gợi ý đã giúp cho công nghệ này được tập trung 
* Học máy phát triển cũng một phần là nhờ việc chúng ta muốn sử dụng những thuật toán của học máy để giải quyết các bài toán về Recommender System

-----
2020
* Và từ những năm đầu tiên , chúng ta đã có Google news , Amazon sử dụng hệ gợi ý được áp dụng , cho đến hiện nay 2020 thì hệ gợi ý xuất hiện ở khắp nơi Tiki , Shoppe , Amazon , Facebook đặc biệt là Netflix, Spotify thành công vang dội là nhờ có một hệ gợi ý được cá nhân hóa tuyệt vời  . Các hệ thống hiểu chúng ta thích xem phim gì , thích nghe nhạc gì hơn cả chính bản thân mình :)
* Đã có những SaaS Recommender System [link ](https://github.com/grahamjenson/list_of_recommender_systems)

![](https://images.viblo.asia/6314fdb0-1b02-4b87-9e8f-3b11151086ea.jpg)

Đã có một số bài viết về Hệ gợi ý tại Viblo , do đó mình chỉ nêu thêm những thứ mình thấy cần thiết và hay khi bản thân tìm hiểu :

* Hệ gợi ý không được cá nhân hóa ( Non personalized Recommendation System)
* => Một trang báo đăng gợi ý cho tất cả mọi người về 10 bộ phim nên xem vào ngày Valentine !!! Đó là một gợi ý không được cá nhân hóa 
* Hệ gợi ý được cá nhân hóa một nửa ( Semi-personalized Recommendation System)
* => Gợi ý mua vé xem ca nhạc vào cuối tuần tùy từng địa điểm 
* Hệ gợi ý được cá nhân hóa (personalized Recommendation System)
* => Tìm hiểu về sở thích của người dùng, những sản phẩm mà người dùng quan tâm . Đưa ra gợi ý cho người dùng về những thứ mà họ thích tuy nhiên họ không biết chúng tồn tại 
* Một điều đáng chú ý nữa :
Khá là khó để phân biệt giữa quảng cáo và gợi ý , vì cả hai đều mang lợi cho người muốn gửi thông điệp . Tuy nhiên khác nhau lớn nhất là việc tính toàn trên dữ liệu của khách hàng , và mong muốn đưa ra những sản phẩm tốt phù hợp hơn cho khách hàng  !!! 

Trong bài viết sau mình sẽ đưa ra các kiến thức cơ bản về các thuật toán gợi ý điểm mạnh , yếu ... , Hiểu cách để thu thập được dữ liệu từ User , Tính toán khi nào thì hệ gợi ý của bạn đang thực hiện tốt ? Và làm sao để build một hệ gợi ý của riêng mình 

THAM KHẢO : 
1. RECOMMENDER SYSTEMS: AN INTRODUCTION


    Author: Dietmar Jannach | Markus Zanker | Alexander Felfernig | Gerhard Friedrich
1. Practical Recommender Systems


    Kim Falk
